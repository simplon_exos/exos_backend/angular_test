import { TestBed } from '@angular/core/testing';

import { AutoCompleteService } from './auto-complete.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('AutoCompleteService', () => {
  /**
   * On crée ici des variables qui contiendrait des objets/instances
   * dont on pourra se resservir dans plusieurs ou tous les tests
   */
  let service: AutoCompleteService;
  let httpController: HttpTestingController;

  beforeEach(() => {
    /**
     * On configure l'environnement de test par rapport à ce qu'utilise
     * le truc qu'on veut tester. Ici on veut tester le AutoCompleteService
     * qui n'a en dépendance que le HttpClient, du coup on injecte dans notre
     * environnement de test le HttpClientTestingModule
     */
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    })
    //On récupère l'instance du service à tester 
    service = TestBed.get(AutoCompleteService);
    /**
     * On récupère une instance de HttpTestingController qui est une
     * classe nous permettant de simuler des requêtes AJAX ainsi que leur
     * réponses
     */
    httpController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    //Après chaque test, on vérifie sur le controller si toutes les requêtes
    //faite lors des test on bien été satisfaites
    httpController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('should returns addresses list', () => {
    /**
     * On crée ici une fausse réponse http correspondant à ce que pourrait
     * nous renvoyer l'API qu'on requête en terme de structure de données.
     * Ici on est sur une api publique, du coup les données renvoyés sont
     * assez nombreuses et pour la plupart inutiles
     */
    const mockResponse = { "type": "FeatureCollection", "version": "draft", 
    "features": [
      { "type": "Feature", "geometry": { "type": "Point", "coordinates": [1.270226, 42.804089] }, "properties": { "label": "test 1", "score": 0.1266208467030546, "id": "09322_g3n5lk", "type": "street", "x": 558360.63, "y": 6190949.51, "importance": 0.26782931373360064, "name": "Serac", "postcode": "09140", "citycode": "09322", "city": "Ustou", "context": "09, Ari\u00e8ge, Occitanie" } }, 
      { "type": "Feature", "geometry": { "type": "Point", "coordinates": [1.270194, 42.803745] }, "properties": { "label": "test 2", "score": 0.11201045709266497, "type": "locality", "importance": 0.26782931373360064, "id": "09322_0053", "name": "Chemin de la Carrero (serac)", "postcode": "09140", "citycode": "09322", "x": 558357.17, "y": 6190911.36, "city": "Ustou", "context": "09, Ari\u00e8ge, Occitanie" } }
    ], "attribution": "BAN", "licence": "ODbL 1.0", "query": "search", "limit": 10 }
    /**
     * On déclenche la méthode du service que l'on souhaite tester et
     * vu que celle ci renvoie un Observable, on se subscribe dessus
     * pour pouvoir vérifier si le retour correspond à ce que l'on attend
     */
    service.getAddresses('example').subscribe(addresses => {
      /**
       * En l'occurrence, on s'attend à avoir comme données :
       * ["test 1", "test 2"]
       * (car c'est ce qu'on a définie dans la mockResponse au niveau
       * des features>properties>label)
       */
      expect(addresses.length).toEqual(2);
      expect(addresses[0]).toEqual('test 1');
      expect(addresses[1]).toEqual('test 2');
    });
    //On utilise le controller pour dire qu'on s'attend à ce que l'url suivante ait été
    //requêtée en GET
    const request = httpController.expectOne('https://api-adresse.data.gouv.fr/search/?q=example&limit=10');
    expect(request.request.method).toEqual('GET');
    //On dit au controller d'envoyer notre fausse résponse 
    request.flush(mockResponse);
  });
});
